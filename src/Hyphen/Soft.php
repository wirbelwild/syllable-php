<?php

namespace BitAndBlack\Syllable\Hyphen;

class Soft extends Entity {
    public function __construct() {
        parent::__construct('shy');
    }
}
