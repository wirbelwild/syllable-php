<?php

namespace BitAndBlack\Syllable\Hyphen;

class Dash extends Text {
    public function __construct() {
        parent::__construct('-');
    }
}
