<?php

namespace BitAndBlack\Syllable\Exception;

use BitAndBlack\Syllable\Exception;

/**
 * Class DirNotReadableException
 * 
 * @package BitAndBlack\Syllable\Exception
 */
class DirNotReadableException extends Exception
{
    /**
     * DirNotReadableException constructor.
     * 
     * @param string $dirName
     */
    public function __construct(string $dirName)
    {
        parent::__construct('Directory "'.$dirName.'" can\'t be read');
    }
}