<?php

namespace BitAndBlack\Syllable\Cache;

/**
 * Single-file cache using JSON format to encode data
 */
class Json extends AbstractCache 
{
    protected function encode($array) 
    {
        return json_encode($array);
    }

    protected function decode($array) 
    {
        return json_decode($array, true);
    }

    /**
     * @param string $language
     * @return string
     */
    protected function getFilename(string $language): string 
    {
        return sprintf('syllable.%s.json', $language);
    }
}
