<?php

namespace BitAndBlack\Syllable\Cache;


abstract class AbstractCache implements CacheInterface 
{
    /**
     * @var string 
     */
    private static $language;
    
    private static $path		= null;
    private static $data		= null;

    abstract protected function encode($array);
    abstract protected function decode($array);

    /**
     * @param string $language
     * @return mixed
     */
    abstract protected function getFilename(string $language);

    public function __construct($path) {
        $this->setPath($path);
    }

    public function setPath($path) {
        if ($path !== self::$path) {
            self::$path = $path;
            self::$data = null;
        }
    }

    /**
     * @return string
     */
    private function filename(): string 
    {
        return sprintf('%s/%s', self::$path, $this->getFilename(self::$language));
    }

    public function open($language) 
    {
        $language = strtolower($language);

        if (self::$language !== $language) {
            self::$language = $language;				
            self::$data		= null;

            $file = $this->filename($language);
            if (is_file($file)) {
                self::$data = $this->decode(file_get_contents($file), true);
            }
        }
    }

    public function close() {
        $file = $this->filename();
        file_put_contents($file, $this->encode(self::$data));
        @chmod($file, 0777);
    }
    
    public function __set($key, $value) {
        self::$data[$key] = $value;
    }

    public function __get($key) {
        return self::$data[$key];
    }

    public function __isset($key) {
        return isset(self::$data[$key]);
    }

    public function __unset($key) {
        unset(self::$data[$key]);
    }				
}
