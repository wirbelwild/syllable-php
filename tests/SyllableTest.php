<?php

namespace BitAndBlack\Syllable\Tests;

use BitAndBlack\Syllable\Exception\DirNotReadableException;
use BitAndBlack\Syllable\Hyphen\Text;
use BitAndBlack\Syllable\Syllable;
use PHPUnit\Framework\TestCase;

/**
 * Class SyllableTest
 * 
 * @package BitAndBlack\Syllable\Tests
 */
class SyllableTest extends TestCase 
{
    /**
     * @var string 
     */
    private $languageDir;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * SyllableTest constructor.
     */
    public function __construct()
    {
        $this->cacheDir = dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'example'.DIRECTORY_SEPARATOR.'cache';
        $this->languageDir = dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'languages';
        parent::__construct();
    }
    
    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testSetLanguage().
     */
	public function testSetLanguage(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    'Su-per-cal-ifrag-ilis-tic-ex-pi-ali-do-cious', 
            $syllable->hyphenateText('Supercalifragilisticexpialidocious')
        );

        $syllable = new Syllable(
            'nl',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
        
		$this->assertEquals(
		    'Su-per-ca-lifra-gi-lis-ti-c-ex-pi-a-li-do-cious', 
            $syllable->hyphenateText('Supercalifragilisticexpialidocious')
        );

        $syllable = new Syllable(
            'fr',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
        
		$this->assertEquals(
		    'Su-per-ca-li-fra-gi-lis-ti-cex-pia-li-do-cious', 
            $syllable->hyphenateText('Supercalifragilisticexpialidocious')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testSetHyphen().
     */
	public function testSetHyphen(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    'Su-per-cal-ifrag-ilis-tic-ex-pi-ali-do-cious', 
            $syllable->hyphenateText('Supercalifragilisticexpialidocious')
        );

        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('/')
        );

		$this->assertEquals(
		    'Su/per/cal/ifrag/ilis/tic/ex/pi/ali/do/cious', 
            $syllable->hyphenateText('Supercalifragilisticexpialidocious')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testGetHyphen().
     */
	public function testGetHyphen(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );

		$this->assertEquals(
		    new Text('-'), 
            $syllable->getHyphen()
        );
		
		$this->assertNotEquals(
		    new Text('+'), 
            $syllable->getHyphen()
        );

        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('/')
        );
        
		$this->assertEquals(
		    new Text('/'), 
            $syllable->getHyphen()
        );
		
		$this->assertNotEquals(
		    new Text('-'), 
            $syllable->getHyphen()
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     */
    public function testThrowsExceptionWhenDirNotFound1(): void
    {
        $this->expectException(DirNotReadableException::class);
        new Syllable(
            'en-us',
            $this->languageDir,
            '',
            new Text('-')
        );
    }

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     */
    public function testThrowsExceptionWhenDirNotFound2(): void
    {
        $this->expectException(DirNotReadableException::class);
        new Syllable(
            'en-us',
            '',
            $this->cacheDir,
            new Text('-')
        );
    }

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testSplitWord().
     */
	public function testSplitWord(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    [';Re', 'dun', 'dan', 't, punc', 'tu', 'a', 'tion...'], 
            $syllable->splitWord(';Redundant, punctuation...')
        );
		
		$this->assertEquals(
		    ['In', 'ex', 'plic', 'a', 'ble'], 
            $syllable->splitWord('Inexplicable')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testSplitText().
     */
	public function testSplitText(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    [';Re', 'dun', 'dant, punc', 'tu', 'a', 'tion...'], 
            $syllable->splitText(';Redundant, punctuation...')
        );
		
		$this->assertEquals(
		    ['In', 'ex', 'plic', 'a', 'ble'], 
            $syllable->splitText('Inexplicable')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testHyphenateWord().
     */
	public function testHyphenateWord(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    ';Re-dun-dan-t, punc-tu-a-tion...', 
            $syllable->hyphenateWord(';Redundant, punctuation...')
        );
		
		$this->assertEquals(
		    'In-ex-plic-a-ble', 
            $syllable->hyphenateWord('Inexplicable')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     * @todo   Implement testHyphenateText().
     */
	public function testHyphenateText(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    ';Re-dun-dant, punc-tu-a-tion...', 
            $syllable->hyphenateText(';Redundant, punctuation...')
        );
		
		$this->assertEquals(
		    'In-ex-plic-a-ble', 
            $syllable->hyphenateText('Inexplicable')
        );
		
		// note that HTML attributes are hyphenated too!
		$this->assertEquals(
		    'Ridicu-lous-ly <b class="un-split-table">com-pli-cat-ed</b> meta-text', 
            $syllable->hyphenateText('Ridiculously <b class="unsplittable">complicated</b> metatext')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     */
	public function testHyphenateHtml(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">'
                ."\n".'<html><body><p>Ridicu-lous-ly <b class="unsplittable">com-pli-cat-ed</b> meta-text</p></body></html>'
                ."\n", 
            $syllable->hyphenateHtml('Ridiculously <b class="unsplittable">complicated</b> metatext')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     */
	public function testCaseInsensitivity(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );
		
		$this->assertEquals(
		    ['IN', 'EX', 'PLIC', 'A', 'BLE'], 
            $syllable->splitText('INEXPLICABLE')
        );
		
		$this->assertEquals(
		    ['in', 'ex', 'plic', 'a', 'ble'], 
            $syllable->splitText('inexplicable')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     */
	public function testHistogramText(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );

		$this->assertSame(
		    [], 
            $syllable->histogramText('.')
        );
		$this->assertSame(
		    [1=>1, 2=>2, 3=>1, 5=>1, 7=>1], 
            $syllable->histogramText('1 is wonder welcome furthermore sophisticated extravagantically.')
        );
	}

    /**
     * @throws \BitAndBlack\Syllable\Exception\DirNotReadableException
     */
	public function testCountWordsText(): void
    {
        $syllable = new Syllable(
            'en-us',
            $this->languageDir,
            $this->cacheDir,
            new Text('-')
        );

		$this->assertSame(
		    0, 
            $syllable->countWordsText('.')
        );
		$this->assertSame(
		    6, 
            $syllable->countWordsText('1 is wonder welcome furthermore sophisticated extravagantically.')
        );
	}
}
