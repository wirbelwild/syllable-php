[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/syllable-php)](http://www.php.net)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/46672f37a7be40e998fb85fcfae36e39)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/syllable-php&amp;utm_campaign=Badge_Grade)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/syllable-php/v/stable)](https://packagist.org/packages/bitandblack/syllable-php)
[![Total Downloads](https://poser.pugx.org/bitandblack/syllable-php/downloads)](https://packagist.org/packages/bitandblack/syllable-php)
[![License](https://poser.pugx.org/bitandblack/syllable-php/license)](https://packagist.org/packages/bitandblack/syllable-php)

# Syllable

PHP Syllable splitting and hyphenation.

## Introduction

Runs in PHP ^7.2 in an object-oriented way.

This library is based on the work by Corey Ballou which is based on the work by Martijn van der Lee which is based on the work by Frank M. Liang [http://www.tug.org/docs/liang/](http://www.tug.org/docs/liang/)
and the many volunteers in the TeX community.

Many languages supported. i.e. english (us/uk), spanish, german, french, dutch,
italian, romanian, russian, etc. 76 languages in total.

Language sources: [http://tug.org/tex-hyphen/#languages](http://tug.org/tex-hyphen/#languages)

__Please note:__ Even though v1.6 is ready for the use with PHP ^7.2 we're still not done refactoring this library.

## Installation 

This library is made for the use with Composer. Add it to your project by running `$ composer require bitandblack/syllable-php`.

## Usage

```php
<?php

use BitAndBlack\Syllable\Syllable;
use BitAndBlack\Syllable\Hyphen\Text;

$syllable = new Syllable(
    'en-us',
    'path/to/language/files',
    'cache/directory',
    new Text('-')
);

// Su-per-cal-ifrag-ilis-tic-ex-pi-ali-do-cious
$syllable->hyphenateText('Supercalifragilisticexpialidocious');

// array(';Re', 'dun', 'dan', 't, punc', 'tu', 'a', 'tion...')
$syllable->splitWord(';Redundant, punctuation...');

// array(';Re', 'dun', 'dant, punc', 'tu', 'a', 'tion...')
$syllable->splitText(';Redundant, punctuation...');
```

## Help 

If you have any questions feel free to contact us under `syllable@bitandblack.com`.