<?php

use BitAndBlack\Syllable\Hyphen\Text;
use BitAndBlack\Syllable\Syllable;

require dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

$syllable = new Syllable(
    'en-us',
    dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'languages',
    dirname(__FILE__, 1).DIRECTORY_SEPARATOR.'cache',
    new Text('-')
);

// Su-per-cal-ifrag-ilis-tic-ex-pi-ali-do-cious
var_dump($syllable->hyphenateText('Supercalifragilisticexpialidocious'));

// array(';Re', 'dun', 'dan', 't, punc', 'tu', 'a', 'tion...')
var_dump($syllable->splitWord(';Redundant, punctuation...'));

// array(';Re', 'dun', 'dant, punc', 'tu', 'a', 'tion...')
var_dump($syllable->splitText(';Redundant, punctuation...'));